# ---------------------------------------------------------------------------------------------------------------------
# Project:       math (https://projecteuler.net)
# Description:   Problem_2
# Version:       1.1.1  --  Python 3.7
# Author:        Stanislav Fux  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fux, 2018
# ---------------------------------------------------------------------------------------------------------------------

"""
Each new term in the Fibonacci sequence is generated
by adding the previous two terms.
By starting with 1 and 2, the first 10 terms will be:
1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...
By considering the terms in the Fibonacci sequence
whose values do not exceed four million, find the sum of the even-valued terms.
"""


def fib_sequence():
    """
    Notice:
    if x == 0:
            return 0
    elif x == 1:
            return 1
    else:
        return fib_sequence(x - 1) + fib_sequence(x - 2)
    """
    result = 0
    a, b = 0, 1
    while True:
        a, b = b, a + b
        if b >= 4_000_000:
            break
        elif b % 2 == 0:
            result = result + b
    return result


print(fib_sequence())

# [DONE]
