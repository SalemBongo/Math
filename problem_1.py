# ---------------------------------------------------------------------------------------------------------------------
# Project:       math (https://projecteuler.net)
# Description:   Problem_1
# Version:       1.0.1  --  Python 3.7
# Author:        Stanislav Fux  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fux, 2018
# ---------------------------------------------------------------------------------------------------------------------

"""
If we list all the natural numbers below 10 that are multiples of 3 or 5,
we get 3, 5, 6 and 9. The sum of these multiples is 23.
Find the sum of all the multiples of 3 or 5 below 1000.
"""


nums = range(1000)
result = list(filter(lambda x: x % 3 == 0 or x % 5 == 0, nums))
sum_result = sum(result)
print(sum_result)

# [DONE]
