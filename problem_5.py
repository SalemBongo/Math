# ---------------------------------------------------------------------------------------------------------------------
# Project:       math (https://projecteuler.net)
# Description:   Problem_5
# Version:       1.0.1  --  Python 3.7
# Author:        Stanislav Fux  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fux, 2018
# ---------------------------------------------------------------------------------------------------------------------

"""
2520 is the smallest number that can be divided
by each of the numbers from 1 to 10 without any remainder.
What is the smallest positive number that is evenly divisible
by all of the numbers from 1 to 20?
"""


def spn():
    a = 1
    for b in range(1, 21):
        if a % b > 0:
            for i in range(1, 21):
                if (a * i) % b == 0:
                    a = a * i
                    break
    return a


print(spn())

# [DONE]
